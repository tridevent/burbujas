﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameState : MonoBehaviour
{
    public static bool checkLifes;
    //public static GameState instance;
    public static int vidas = 3;
    public static int puntos = 0;
    public static int altopuntaje = 0;
    public static bool idioma = false;//false español ,true ingles
    Font Fuente;
    private string puntajealto;
    //---------------------------------
    private int contador = 60;
    //----------------------------------
    GameObject objeto1;
    GameObject objeto2;
    GameObject objeto3;
    Sprite rojo;
    Sprite amarillo;
    Sprite verde;
    /*private GameState ()
    {
        instance = this;
    }*/

    void Start()
    {
        if (idioma == true)
        {
            puntajealto = "high score: ";
        }
        if (idioma == false)
        {
            puntajealto = "mejor puntuacion: ";
        }
        InvokeRepeating("timer", 1, 1);
    }

    void timer()
    {
        Generator generator = new Generator();
        generator.enemigo--;
        contador--;
        if (contador <= 0)
        {
            Bubble.velocidad = Bubble.velocidad + 0.5f;
            generator.velGenerado = generator.velGenerado - 0.3f;
            contador = 60;
        }
    }
    void Update()
    {
        if(checkLifes)
        {
            CheckLifes();
            checkLifes = false;
        }
        
    }

    public void CheckLifes()
    {
        switch (vidas)
        {
            case 3:
                objeto1.GetComponent<Image>().sprite = verde;
                objeto2.GetComponent<Image>().sprite = verde;
                objeto3.GetComponent<Image>().sprite = verde;
                break;
            case 2:
                objeto1.GetComponent<Image>().sprite = amarillo;
                objeto2.GetComponent<Image>().sprite = amarillo;
                objeto3.SetActive(false);
                break;
            case 1:
                objeto1.GetComponent<Image>().sprite = rojo;
                objeto2.SetActive(false);
                break;
            case 0:
                Bubble.velocidad = 1.0f;
                vidas = 3;
                if (altopuntaje < puntos)
                {
                    altopuntaje = puntos;
                    PlayerPrefs.SetInt("Altopuntaje", altopuntaje);
                }
                puntos = 0;
                PlayerPrefs.SetInt("Puntos", puntos);
                Application.LoadLevel("Perder");
                break;
        }
    }
}
