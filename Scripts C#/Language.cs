﻿using UnityEngine;
using System.Collections;

public class Language : MonoBehaviour {
    
    public void SetSpanish()
    {
        GameState.idioma = false;
        GoGame();
    }
    public void SetEnglish()
    {
        GameState.idioma = true;
        GoGame();
    }
    void GoGame()
    {
        Application.LoadLevel("MenuPrincipal");
    }
}
