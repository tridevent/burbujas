﻿using UnityEngine;
using System.Collections;

public class Generator : MonoBehaviour
{
    public Transform objeto;
    public Transform objetochico;
    public Transform objMalo;
    private int tamano = 0;
    public float velGenerado = 1;
    public int enemigo = 15;
    public int reaparicion = 15;
    void Start()
    {
        InvokeRepeating("generar", 1, velGenerado);
        reaparicion = Random.Range(10, 20);
    }
    void generar()
    {
        if (enemigo > 0)
        {
            tamano = Random.Range(0, 2);
            if (tamano == 0)
            {
                Instantiate(objeto, new Vector3(0, 0, 0), transform.rotation);
            }
            if (tamano == 1)
            {
                Instantiate(objetochico, new Vector3(0, 0, 0), transform.rotation);
            }
        }
        else
        {
            Instantiate(objMalo, new Vector3(0, 0, 0), transform.rotation);
            enemigo = reaparicion;
        }
    }
}