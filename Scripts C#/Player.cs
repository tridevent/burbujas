﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour {
    private Touch touch;

	void Start () {
	
	}

	void Update () {
        if(Input.GetMouseButtonUp(0))
        {
            CollisionBubbles(Input.mousePosition);
        }
        if(touch.tapCount>0)
        {
            CollisionBubbles(touch.position);
        }
    }
    void CollisionBubbles(Vector2 target)
    {
        RaycastHit2D ray = Physics2D.Raycast(Vector2.zero, target);
        if (ray.collider.tag == "bubble")
        {
            ray.collider.gameObject.GetComponent<Bubble>().PopBubble();
        }
        if (ray.collider.tag == "bubbleDamage")
        {
            ray.collider.gameObject.GetComponent<Bubble>().PopBubbleDamage();
        }
    }
}
