﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CountDown : MonoBehaviour
{
    public Sprite textura1;
    public Sprite textura2;
    public Sprite textura3;
    public Sprite ya;
    public Transform objeto;
    private int contador = 4;

    void Start()
    {
        GetComponent<Image>().sprite = textura3;
        InvokeRepeating("contar", 1, 1);
    }

    void contar()
    {

        if (contador == 4)
        {
            GetComponent<Image>().sprite = textura3;
        }
        if (contador == 3)
        {
            GetComponent<Image>().sprite = textura2;
        }
        if (contador == 2)
        {
            GetComponent<Image>().sprite = textura1;
        }
        if (contador == 1)
        {
            GetComponent<Image>().sprite = ya;
            Instantiate(objeto,  new Vector3(0, 0, 0), transform.rotation);
        }
        if (contador == 0)
        {
            Destroy(gameObject);
        }
        contador--;
    }
}
