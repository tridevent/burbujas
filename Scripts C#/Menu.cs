﻿using UnityEngine;
using System.Collections;

public class Menu : MonoBehaviour
{
    static bool pausado = false;
    private bool check1 = false;
    GameObject pausaObj;
    GameObject salirObj;

    void Start()
    {
        pausaObj = GameObject.Find("Reanudar");
        pausaObj.SetActive(false);
        salirObj = GameObject.Find("Menu");
        salirObj.SetActive(false);
        //-------------------------------------------------------
    }

    public void MenuPress()
    {
        pausado = false;
        Application.LoadLevel("MenuPrincipal");
    }

    public void CheckPause()
    {
        //-----------------------------------------------------------------------
        if (pausado == true)
        {
            if (check1 == false)
            {
                Music.sonido = 1;
                Time.timeScale = 0.0f;
                pausaObj.SetActive(true);
                salirObj.SetActive(true);
                check1 = true;
            }
        }
        if (pausado == false)
        {
            Time.timeScale = 1;
            if (check1 == true)
            {
                Music.sonido = 0;
                pausaObj.SetActive(false);
                salirObj.SetActive(false);
                check1 = false;
            }
        }
        //---------------------------------------------------------------------------
    }
    public void ResumePress()
    {
        pausado = false;
    }
}
