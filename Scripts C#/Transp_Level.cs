﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Transp_Level : MonoBehaviour {
    private int contador = 0;
    public int espera = 0;
    public bool cargarNivel = false;
    public bool salirJuego = false;
    public string nivel;
    public bool direccionInvertida = false;
    public float transparencia = 0.05f;
    private float color;
    private bool termino = false;
    private Color transColor;

    void Start()
    {
        transColor = Color.white;
        transColor.a = 0f;
        termino = false;
        if (direccionInvertida == false)
        {
            color = 0;
        }
        if (direccionInvertida == true)
        {
            color = 1;
        }
        Image imagen = gameObject.GetComponent<Image>();//transparente
        imagen.color = transColor;
        GetComponent<Image>().color = imagen.color;
        if (cargarNivel == true || salirJuego == true)
        {
            InvokeRepeating("tiempo", 1, 1);
        }
        InvokeRepeating("transparencia2", 1, 0.1f);
    }

    void tiempo()
    {
        if (contador < espera)
        {
            contador++;
        }
        if (contador == espera)
        {
            contador = 0;
            if (salirJuego == true)
            {
                print("Sali");
                Application.Quit();
            }
            if (cargarNivel == true)
            {
                Application.LoadLevel(nivel);
            }
        }
    }
    void transparencia2()
    {
        if (termino == true)
        {
            //objeto.SetActive(false);
            Destroy(gameObject);
        }
        if (direccionInvertida == false)
        {
            if (color <= 2)
            {
                color += transparencia;
            }
            else
            {
                if (cargarNivel == false && salirJuego == false)
                {
                    termino = true;
                }
            }
        }
        if (direccionInvertida == true)
        {
            if (color >= 0)
            {
                color -= transparencia;
            }
            else
            {
                if (cargarNivel == false && salirJuego == false)
                {
                    termino = true;
                }
            }
        }
        transColor.a = color;
        Image imagen = gameObject.GetComponent<Image>();// a opaco
        imagen.color = transColor;
        GetComponent<Image>().color = imagen.color;
    }
}
