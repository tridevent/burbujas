﻿using UnityEngine;
using System.Collections;

public class Music : MonoBehaviour
{
    AudioClip musicaPausa;
    AudioClip musicaOriginal;
    AudioClip musicaPerder;
    AudioClip[] colMusica;
    private int aleatorio = 0;
    public static short sonido = 0; //0: original 1:pausa 2: perder
                             //static short estadoExtra=0;//0: original 1:pausa 2: ganar 3: perder 
    void Start()
    {
        sonido = 0;
        aleatorio = Random.Range(0, colMusica.Length);
        musicaOriginal = colMusica[aleatorio];
        GetComponent<AudioSource>().clip = musicaOriginal;
        GetComponent<AudioSource>().Play();
    }

    void Update()
    {
        if (sonido == 0)
        {
            GetComponent<AudioSource>().clip = musicaOriginal;
            GetComponent<AudioSource>().Play();
            GetComponent<AudioSource>().volume = 1;
            sonido = 5;
        }
        if (sonido == 1)
        {
            GetComponent<AudioSource>().clip = musicaPausa;
            GetComponent<AudioSource>().Play();
            GetComponent<AudioSource>().volume = 1;
            sonido = 5;
        }
        if (sonido == 2)
        {
            GetComponent<AudioSource>().clip = musicaPerder;
            GetComponent<AudioSource>().Play();
            GetComponent< AudioSource > ().volume = 1;
            sonido = 5;
        }
    }
}