﻿using UnityEngine;
using System.Collections;

public class Bubble : MonoBehaviour
{
    public static float velocidad = 1.0f;
    AudioClip sonido;
    private bool check = false;
    private bool bandera = false;
    static bool check2 = false;
    Touch touch;
    //---------------------------------------------
    Touch toque;
    //public GameObject Imagen;
    public Sprite Textura;
    //----------------------------------------------
    public GameObject[] respawning;
    private int random;

    void Start()
    {
        random = Random.Range(0, respawning.Length);
        //---------------------------------------------------
        gameObject.transform.position = respawning[random].transform.position;
        //---------------------------------------------------
        check = false;
        bandera = false;
        //--------------------------------------------------------
        InvokeRepeating("SecondUpdate", 1, 0.01f);
    }

    void SecondUpdate()
    {
        if (bandera == false)
        {
            transform.Translate(new Vector3(0, 1, 0));
        }
    }

    void Update()
    { 
        //--------------------------------------------------------------------//
        if (touch.phase == TouchPhase.Ended || Input.GetMouseButtonUp(0))
        {
            check2 = false;
        }
    }
    IEnumerable Activar()
    {
        bandera = true;
        /*Imagen.*/GetComponent<SpriteRenderer>().sprite = Textura;
        if (check == false)
        {
            GetComponent<AudioSource>().clip = sonido;
            GetComponent<AudioSource>().Play();
            check = true;
        }
        yield return new WaitForSeconds(0.5f);
        Destroy(gameObject);
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if(col.gameObject.tag=="damage")
        {
            if(this.tag=="bubble")
            {
                GameState.vidas--;
                GameState.checkLifes = true;
            }
            if(this.tag=="bubbleDamage")
            {
                GameState.puntos+=3;
            }
            Destroy(gameObject);
        }
    }

    public void PopBubble()
    {
        if (check2 == false)
        {
            GameState.puntos++;
            check2 = true;
            Activar();
        }
    }
    public void PopBubbleDamage()
    {
        if (check2 == false)
        {
            GameState.vidas--;
            GameState.checkLifes=true;
            check2 = true;
            Activar();
        }
    }
}